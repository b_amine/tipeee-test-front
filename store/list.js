export const state = () => ({
    listItems: [
        {
            name: "Maliki",
            slug: "maliki"
        },
        {
            name: "Thinkerview",
            slug: "thinkerview"
        },
        {
            name: "Yatuu",
            slug: "yatuu"
        },
        {
            name: "François Theurel",
            slug: "francois-theurel"
        }
    ],
    nbElementsToShow: 4
})

export const mutations = {
    add(state, item) {
        state.listItems.unshift(item)
    },
    updateNbElementsToShow(state, nb){
        state.nbElementsToShow = nb
    }
}

